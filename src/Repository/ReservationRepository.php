<?php

namespace App\Repository;

use App\Entity\Reservation;
use App\Tools\TypeStatus;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Uid\UuidV4;

/**
 * @extends ServiceEntityRepository<Reservation>
 *
 * @method Reservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservation[]    findAll()
 * @method Reservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reservation::class);
    }

    public function findReservationOpen() : array {
        return $this->createQueryBuilder('r')
            ->andWhere('r.status in(\'open\', \'Wait\')')
            ->getQuery()
            ->getResult()
        ;
    }

    public function confirmAndUpdateAnotherReservation(Reservation $res, MailerInterface $mailer) : bool {
        
        $res->setStatus(TypeStatus::Confirmed);
        $res->setUpdatedAt(new DateTime());
        $this->getEntityManager()->persist($res);
        $this->getEntityManager()->flush();

        $email = (new Email)
        ->from("ynov@ynov.com")
        ->to("ynov@ynov.com")
        ->subject("Mail de Confirmation")
        ->text("C'est le mail de confirmation de la réservation du films");

        $mailer->send($email);


        $updateRes = $this->createQueryBuilder('r')
            ->andWhere('r.rank > :rank')
            ->andWhere('r.status in(\'open\', \'Wait\')')
            ->setParameter("rank", $res->getRank())
            ->getQuery()
            ->getResult();

        if (!empty($updateRes)) {
            foreach ($updateRes as $ures) {
                if (new DateTime() > $ures->getExpiresAt()) $ures->setStatus(TypeStatus::Expired); 
                else $ures->setStatus(TypeStatus::Open);
                $ures->setUpdatedAt(new DateTime());
                $this->getEntityManager()->persist($ures);
                $this->getEntityManager()->flush();
            }
        }
        
        return true;
    }



    //    /**
    //     * @return Reservation[] Returns an array of Reservation objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Reservation
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
