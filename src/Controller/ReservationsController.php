<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Tools\EnumMethodHttp;
use App\Tools\OauthLib;
use App\Tools\WorkerClass;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\UuidV4;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[Route('/reservations', name: 'app_reservations')]
class ReservationsController extends AbstractController
{

    public function __construct(
        private SerializerInterface $serializer,
        private EntityManagerInterface $entityManager,
        private HttpClientInterface $http,
        private MailerInterface $mailer
        )
        {
        }
        
    #[Route('/', name: '_index')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ReservationsController.php',
        ]);
    }

    #[Route('/{uid}', name: '_details', methods: ['GET'])]
    public function getReservationDetails(mixed $uid): JsonResponse
    {
        try {
            // if ($request->headers->has("Authentication")) {
            //     $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
            //     if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
            //     $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
            //     foreach (explode(", ", "ROLE_USER, ROLE_ADMIN") as $role) {
            //         if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
            //     }
            // }
            // else throw new Exception("Pas d'authentication", 404);
            
            // Recherche la réservation par son UID
            $reservation = $this->entityManager->getRepository(Reservation::class)->findOneBy(['uid' => $uid]);

            if ($reservation === null) {
                throw new \Exception("La réservation est introuvable", 404);
            }

            // Construit les données de réponse
            $responseData = [
                'uid' => $reservation->getUid(),
                'rank' => $reservation->getRank(),
                'status' => $reservation->getStatus(),
                'createdAt' => $reservation->getCreatedAt(),
                'updatedAt' => $reservation->getUpdatedAt(),
                'expiresAt' => $reservation->getExpiresAt(),
            ];

            // Renvoie les détails de la réservation en JSON
            return $this->json($responseData);
        } catch (\Exception $e) {
            // En cas d'erreur, renvoie un JSON avec le message d'erreur et le code HTTP approprié
            return $this->json(['error' => $e->getMessage()], $e->getCode());
        }
    }

    #[Route('/{uid}/confirm', name: '_confirm', methods: ['POST'], stateless: true)]
    public function postReservationConfirm(Request $request, mixed $uid) : Response {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_USER, ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);
            if (UuidV4::isValid($uid)) throw new Exception("UID invalid", 404);
            
            $reservation = $this->entityManager->getRepository(Reservation::class)->findOneBy(["uid" => $uid]);
            if (empty($reservation)) throw new Exception("Aucune Reservation", 404);
            if ($user["roles"] !== "ROLE_ADMIN" && $user["uid"] != $reservation->getOwner()) throw new Exception("Utilisateur invalid", 404);
            if (new DateTime() > $reservation->getExpiresAt()) throw new Exception("Expired", 410);
            $this->entityManager->getRepository(Reservation::class)->confirmAndUpdateAnotherReservation($reservation, $this->mailer);
            return $this->json("Confirmed", 210);            
        } catch (\Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }
}
    // $worker = new WorkerClass($this->entityManager);
    // $worker->Connection();
    // $worker->declareQueue('test', false, true, false, false);
    // $worker->verifReservationUID($uid);
    // $test = array();
    // $worker->getData(function ($msg) {
    //     $test[] = $msg->getBody();
    //     $test[] = $msg;
    //     // $msg->ack();
    // });
    // // $worker->Test();
    // $worker->ConnectionClose();
    
    // return $this->json($test, 200);