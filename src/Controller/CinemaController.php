<?php

namespace App\Controller;

use App\Entity\Cinema;
use App\Entity\Room;
use App\Entity\Sceance;
use App\Tools\EnumMethodHttp;
use App\Tools\OauthLib;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\UuidV4;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[Route('/cinema', name: 'app_cinema', stateless: true)]
class CinemaController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer,
        private EntityManagerInterface $entityManager,
        private HttpClientInterface $http
    )
    {
    }


    #[Route('/index', name: '_index', stateless: true)]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/CinemaController.php',
        ]);
    }

    #[Route('/', name: '_list', methods: ['GET'], stateless: true)]
    public function listCinemas(Request $request): Response
    {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_USER, ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);
    
            $data = [];
            foreach ($this->entityManager->getRepository(Cinema::class)->findAll() as $cinema) {
                $data[] = [
                    'uid' => $cinema->getUid(),
                    'name' => $cinema->getName(),
                    'createdAt' => $cinema->getCreatedAt(),
                    'updatedAt' => $cinema->getUpdatedAt()
                ];
            }
    
            return $this->json($data, 200);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    #[Route('/{uid}', name: '_show', methods: ['GET'], stateless: true)]
    public function showCinema(Request $request, mixed $uid): JsonResponse
    {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_USER, ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);

            if (!UuidV4::isValid($uid)) throw new Exception("Erreur Interne", 500);

            $cinema = $this->entityManager->getRepository(Cinema::class)->findOneBy(['uid' => $uid]);
            
            if (empty($cinema)) throw new Exception("Cinema not Found", 404);
            
            
            $data = [
                'uid' => $cinema->getUid(),
                'name' => $cinema->getName(),
                'createdAt' => $cinema->getCreatedAt(),
                'updatedAt' => $cinema->getUpdatedAt()
            ];
            
            return $this->json($data, 200);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }
                
    #[Route('/', name: '_create', methods: ['POST'], stateless: true)]
    public function createCinema(Request $request): Response
    {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);


            if (empty($request->getContent())) throw new Exception("Le contenu de l'objet cinéma dans le body est invalide", 422);
            $content = json_decode($request->getContent(), true);

            if ((UuidV4::isValid($content["uid"]) && empty($content["uid"])) && (empty($content["name"]) && is_string($content["name"]) && strlen($content["name"]) > 128)) throw new Exception("Erreur Interne", 500);
            
            $cinema = new Cinema();
            $cinema->setUid(new UuidV4($content['uid']));
            $cinema->setName($content['name']);
            $cinema->setCreatedAt(new \DateTime());
            $cinema->setUpdatedAt(new \DateTime());
            
            $this->entityManager->persist($cinema);
            $this->entityManager->flush();
            
            return $this->json([
                'uid' => $cinema->getUid(),
                'name' => $cinema->getName(),
                'createdAt' => $cinema->getCreatedAt(),
                'updatedAt' => $cinema->getUpdatedAt()
            ], 201);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    #[Route('/{uid}', name: '_update', methods: ['PUT'], stateless: true)]
    public function updateCinema(mixed $uid, Request $request): Response
    {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);

            if (!UuidV4::isValid($uid)) throw new Exception("Erreur Interne", 500);

            $cinema = $this->entityManager->getRepository(Cinema::class)->findOneBy(['uid' => $uid]);
            
            if (empty($cinema)) throw new Exception("Cinema not Found", 404);
            
            if (empty($request->getContent())) throw new Exception("Le contenu de l'objet cinéma dans le body est invalide", 422);
            $content = json_decode($request->getContent(), true);   

            $cinema->setName($content['name']);
            $cinema->setUpdatedAt(new \DateTime());
            
            $this->entityManager->persist($cinema);
            $this->entityManager->flush();
            
            return $this->json([
                'uid' => $cinema->getUid(),
                'name' => $cinema->getName(),
                'createdAt' => $cinema->getCreatedAt(),
                'updatedAt' => $cinema->getUpdatedAt()
            ], 200);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    #[Route('/{uid}', name: '_delete', methods: ['DELETE'], stateless: true)]
    public function deleteCinema(mixed $uid, Request $request): JsonResponse
    {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);

            if (!UuidV4::isValid($uid)) throw new Exception("Erreur Interne", 500);
            
            $cinema = $this->entityManager->getRepository(Cinema::class)->findOneBy(['uid' => $uid]);
            
            if (empty($cinema)) throw new Exception("Cinema not Found", 404);

            $this->entityManager->remove($cinema);
            $this->entityManager->flush();

            return $this->json("Suppresion faite", 204);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    #[Route('/{cinemaUid}/rooms', name: '_cinema_rooms', stateless: true, methods: ['GET'], options: ['cinemaUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/'])]
    public function getRoomsCinema(Request $request, mixed $cinemaUid) : Response {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_USER, ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);

            if (!UuidV4::isValid($cinemaUid)) throw new Exception("Id Cinéma invalide", 500);
            $cinema = $this->entityManager->getRepository(Cinema::class)->findOneBy(["uid" => $cinemaUid]);
            if (empty($cinema)) throw new Exception("Le cinéma est inconnu", 404);
            $rooms = array();
            
            if (!$cinema->getRoom()->isEmpty()) {
                foreach ($cinema->getRoom() as $c) {
                    $rooms[] = array(
                        "uid" => $c->getUid(),
                        "seats" => $c->getSeats(),
                        "name" => $c->getName(),
                        "createdAt" => $c->getCreatedAt(),
                        "updatedAt" => $c->getUpdatedAt()
                    );
                }
            }
            else $rooms = "Pas de sale";
                
            return $this->json($rooms, 200);    
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
        
    }

    #[Route('/{cinamaUid}/rooms/{uid}', name: '_cinema_room', stateless: true, methods: ['GET'], options: ['cinemaUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', 'uid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/'])]
    public function getRoomCinema(Request $request, mixed $cinemaUid, mixed $uid) : Response {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_USER, ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            // else throw new Exception("Pas d'authentication", 404);

            if (!UuidV4::isValid($cinemaUid) && !UuidV4::isValid($uid)) throw new Exception("Erreur Interne", 500);
            $cinema = $this->entityManager->getRepository(Cinema::class)->findOneBy(["uid" => $cinemaUid]);
            if (empty($cinema)) throw new Exception("Pas de résultat de recherche", 204);
            $room = $this->entityManager->getRepository(Room::class)->findOneBy(["uid" => $cinema->getRoom()]);
            if (empty($room)) throw new Exception("Pas de résultat de recherche", 204);
            
            return $this->json([
                "uid" => $room->getUid(),
                "seats" => $room->getSeats(),
                "name" => $room->getName(),
                "createdAt" => $room->getCreatedAt(),
                "updatedAt" => $room->getUpdatedAt()
            ], 200);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    #[Route('/{cinemaUid}/rooms', name: '_cinema_room_post', stateless: true, methods: ['POST'], options: ['cinemaUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/'])]
    public function setRoomCinema(Request $request, mixed $cinemaUid) : Response {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);

            if (empty($request->getContent())) throw new Exception("Erreur Interne", 500);
            $content = json_decode($request->getContent(), true);
            if (empty($content) && empty($content["name"])) throw new Exception("Les paramètres de recherche sont invalide", 400);
            
            if (strlen($content["name"]) > 120) throw new Exception("Les paramètres de recherche sont invalide", 400);
            
            $cinema = $this->entityManager->getRepository(Cinema::class)->findOneBy(["uid" => $cinemaUid]);
            if (empty($cinema)) throw new Exception("La sale est inconnu", 404);
        
            $room = new Room();
            $room->setName($content['name']);
            $room->setSeats(30);
            $room->setUid(new UuidV4);
            $room->setCreatedAt(new \DateTime());
            $room->setUpdatedAt(new \DateTime());

            $this->entityManager->persist($room);
            $this->entityManager->flush();

            $cinema->addRoom($room);
            $this->entityManager->persist($cinema);
            $this->entityManager->flush();

            
            return $this->json([
                "uid" => $room->getUid(),
                "seats" => $room->getSeats(),
                "name" => $room->getName(),
                "createdAt" => $room->getCreatedAt(),
                "updatedAt" => $room->getUpdatedAt()
            ], 201);
            
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    #[Route('/{cinemaUid}/rooms/{roomUid}/sceances/{uid}', name: '_cinema_room_put', stateless: true, methods: ['PUT'], options: [
        'cinemaUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', 
        'roomUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/',
        'uid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/'
    ])]
    public function putRoomCinema(Request $request, mixed $cinemaUid, mixed $roomUid, mixed $uid) : Response {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);

            
            if (empty($request->getContent())) throw new Exception("Error Processing Request", 400);
            $content = json_decode($request->getContent());
            if (empty($content) && empty($content["movie"]) && empty($content["date"])) throw new Exception(" Le contenu de l'objet cinema dans le body est invalide", 422);

            if (!UuidV4::isValid($cinemaUid) && !UuidV4::isValid($roomUid) && !UuidV4::isValid($uid)) throw new Exception("Erreur Interne", 500);
            $cinema = $this->entityManager->getRepository(Cinema::class)->findOneBy(["uid" => $cinemaUid]);
            if (empty($cinema)) throw new Exception("Pas de résultat de recherche", 404);
            $room = $this->entityManager->getRepository(Room::class)->findOneBy(["uid" => $cinema->getRoom()]);
            if (empty($room)) throw new Exception("Pas de résultat de recherche", 204);
            $sceances = $this->entityManager->getRepository(Sceance::class)->findOneBy(["uid" => $room->getSceance()]);
            if (empty($sceances)) throw new Exception("Pas de résultat de recherche", 204);
            
            return $this->json([
                "uid" => $sceances->getUid(),
                "movie" => $sceances->getMovies(),
                "date" => $sceances->getDate()
            ]);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    #[Route('/{cinemaUid}/rooms/{roomUid}/seances', name: '_seances_list', methods: ['GET'], options: [
        'cinemaUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', 
        'roomUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/'
    ])]
    public function getSeances(Request $request, mixed $cinemaUid, mixed $roomUid): JsonResponse
    {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_USER, ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);

            if (!UuidV4::isValid($cinemaUid) && !UuidV4::isValid($roomUid)) throw new Exception("Erreur Interne", 500);

            $sceances = $this->entityManager->getRepository(Sceance::class)->findBy(['cinemaUid' => $cinemaUid, 'roomUid' => $roomUid]);

            if (empty($sceances)) throw new Exception("Aucune Scéance", 404);
            

            $responseData = [];
            foreach ($sceances as $sceance) {
                $responseData[] = [
                    'uid' => $sceance->getUid(),
                    'movie' => $sceance->getMovies(),
                    'date' => $sceance->getDate(),
                ];
            }

            return $this->json($responseData);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    #[Route('/cinema/{cinemaUid}/rooms/{roomUid}/seances', name: '_seances_create', methods: ['POST'], options: [
        'cinemaUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', 
        'roomUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/'
    ])]
    public function createSeance(Request $request, mixed $cinemaUid, mixed $roomUid): JsonResponse
    {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);

            if (!UuidV4::isValid($cinemaUid) && !UuidV4::isValid($roomUid)) throw new Exception("Erreur Interne", 500);
            if (empty($request->getContent())) throw new Exception("Le contenu de l'objet film dans le body est invalide", 422);
            $requestData = json_decode($request->getContent(), true);

            if (!isset($requestData['movie']) || !isset($requestData['date']) && !UuidV4::isValid($requestData['movie'])) throw new Exception('Les données requises (movie, date) sont manquantes.', 400);

            $cinema = $this->entityManager->getRepository(Cinema::class)->find(["uid" => $cinemaUid]);
            if (empty($cinema)) throw new Exception("Cinéma non trouvé", 404);
            $room = $this->entityManager->getRepository(Room::class)->findOneBy(["uid" => $roomUid]);
            if (empty($room)) throw new Exception("Salle non trouvé", 404);
            
            
            $sceance = new Sceance();
            $sceance->setMovies(new UuidV4($requestData['movie']));
            $sceance->setDate(new DateTime($requestData['date']));
            $sceance->setUid(new UuidV4());
            
            $this->entityManager->persist($sceance);
            $this->entityManager->flush();
            
            $room->setSceance($sceance);
            $this->entityManager->persist($room);
            $this->entityManager->flush();

            $cinema->addRoom($room);
            $this->entityManager->persist($cinema);
            $this->entityManager->flush();
            
            

            return $this->json([
                'uid' => $sceance->getUid(),
                'movie' => $sceance->getMovies(),
                'date' => $sceance->getDate()->format('Y-m-d H:i:s'),
            ]);
        } catch (Exception $e) {
            return $this->json($e->getMessage(), $e->getCode());
        }
    }

    #[Route('/cinema/{cinemaUid}/rooms/{roomUid}/seances/{uid}', name: '_seances_delete', methods: ['DELETE'], options: [
        'cinemaUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', 
        'roomUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/',
        'uid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/'
    ])]
    public function deleteSeance(Request $request, mixed $cinemaUid, mixed $roomUid, mixed $uid): JsonResponse
    {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404])) throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"])) throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);

            $sceance = $this->entityManager->getRepository(Sceance::class)->findOneBy(['uid' => $uid]);

            if (empty($sceance)) throw new Exception('La séance spécifiée n\'existe pas.', 404);

            if (!UuidV4::isValid($cinemaUid) && !UuidV4::isValid($roomUid) && !UuidV4::isValid($uid)) throw new Exception("Erreur Interne", 500);

            $cinema = $this->entityManager->getRepository(Cinema::class)->findOneBy(["uid" => $cinemaUid]);
            if (empty($cinema)) throw new Exception("Cinéma non trouvé", 404);
            
            $verifRoom = count(array_filter($cinema->getRoom()->toArray(), function($room) use ($roomUid) {
                return (new UuidV4($room->getUid())) !== (new UuidV4($roomUid));
            }));

            if ($verifRoom > 0) throw new Exception("Aucune salle ne corespond au cinéma", 400);
            
            $room = $this->entityManager->getRepository(Room::class)->findOneBy(["uid" => $roomUid]);
            if (empty($room)) throw new Exception("Salle non trouvé", 404);
            
            if (new UuidV4($room->getSceance()->getUid()) !== new UuidV4($uid)) throw new Exception("Aucune sceane pour cette salle", 400);
            
            


            $this->entityManager->remove($sceance);
            $this->entityManager->flush();

            return $this->json(['message' => 'La séance a été supprimée avec succès.']);
        } catch (Exception $e) {
            return $this->json(['error' => $e->getMessage()], $e->getCode());
        }
    }
}
