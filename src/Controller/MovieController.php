<?php

namespace App\Controller;

use App\Tools\libMovis;
use App\Tools\RabbitMQLib;
use App\Tools\WorkerClass;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Entity\Reservation;
use App\Entity\Room;
use App\Entity\Sceance;
use App\Tools\EnumMethodHttp;
use App\Tools\OauthLib;
use App\Tools\TypeStatus;
use DateTime;
use Symfony\Component\Uid\UuidV4;

#[Route('/movies', name: 'app_movie', stateless: true)]
class MovieController extends AbstractController
{

    public function __construct(
        private SerializerInterface $serializer,
        private EntityManagerInterface $entityManager,
        private HttpClientInterface $http
    ) {
    }

    #[Route('/index', name: '_index', stateless: true)]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/MovieController.php',
        ]);
    }

    #[Route('/{movieUid}/reservations', name: '_movie_reservations', methods: ['GET'])]
    public function getMovieReservations(Request $request, string $movieUid): JsonResponse
    {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404]))
                    throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_USER, ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"]))
                        throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            // else throw new Exception("Pas d'authentication", 404);


            $reservations = $this->entityManager->getRepository(Reservation::class)->findBy(['movieUid' => $movieUid]);

            // Vous devez formater les données des réservations selon vos besoins
            $formattedReservations = array();
            foreach ($reservations as $reservation) {
                $formattedReservations[] = [
                    'uid' => $reservation->getUid(),
                    'rank' => $reservation->getRank(),
                    'status' => $reservation->getStatus(),
                    'createdAt' => $reservation->getCreatedAt(),
                    'updatedAt' => $reservation->getUpdatedAt(),
                    'expiresAt' => $reservation->getExpiresAt(),
                ];
            }

            return $this->json($formattedReservations, Response::HTTP_OK);
        } catch (\Throwable $th) {
            return $this->json(['error' => $th->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    #[Route('/{movieUid}/reservations', name: '_reservation', stateless: true, options: ['movieUid' => '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/'], methods: ['POST'])]
    public function reservations(Request $request, mixed $movieUid): Response
    {
        try {
            if ($request->headers->has("Authentication")) {
                $oauth = (new OauthLib($this->http))->setAuthentication($request->headers->get("Authentication"));
                if (in_array($oauth->setMethod(EnumMethodHttp::HEAD)->getHeaderOauth()->getStatusCode(), [401, 404]))
                    throw new Exception("Il faut vous connecter", 404);
                $user = json_decode($oauth->setMethod(EnumMethodHttp::GET)->getHeaderOauth()->getContent(), true);
                foreach (explode(", ", "ROLE_ADMIN") as $role) {
                    if (!in_array($role, $user["roles"]))
                        throw new Exception("Vous n'avais pas le droit pour y accéder", 403);
                }
            }
            else throw new Exception("Pas d'authentication", 404);


            if (empty ($request->getContent()))
                throw new Exception("Le contenu de l'objet reservation dans le body est invalide", 422);
            $content = json_decode($request->getContent(), true);
            extract($content);
            $contentType = $request->headers->get('content-type');

            $movie = ((new libMovis($this->entityManager, $this->http))->setMovisUuid($movieUid))->getMovies();
            if (empty ($movie))
                throw new Exception("Film non trouvé", 404);

            if (empty ($sceance) && UuidV4::isValid($sceance) && empty ($nbSeats) && is_numeric($nbSeats) && $nbSeats <= 0 && empty ($room) && UuidV4::isValid($room))
                throw new Exception("Le contenu de l'objet reservation dans le body est invalide", 422);

            $sceances = $this->entityManager->getRepository(Sceance::class)->findOneBy(["uid" => $sceance]);
            if (empty ($sceances))
                throw new Exception("Sceance non trouvé", 422);

            $rooms = $this->entityManager->getRepository(Room::class)->findOneBy(["uid" => $room]);
            if (empty ($rooms))
                throw new Exception("Salle non trouvé", 422);

            $reservation = new Reservation();
            $reservation->setUid(new UuidV4());
            $reservation->setRank(count($this->entityManager->getRepository(Reservation::class)->findReservationOpen()) > 0 ? count($this->entityManager->getRepository(Reservation::class)->findReservationOpen()) + 1 : 1);
            $reservation->setStatus(TypeStatus::Open);
            $reservation->setSeats($nbSeats);
            $reservation->setRoom($rooms);
            $reservation->setSeance($sceances);
            $reservation->setCreatedAt(new DateTime());
            $reservation->setUpdatedAt(new DateTime());
            $reservation->setExpiresAt((new DateTime())->add(new DateInterval("P1D")));
            $reservation->setOwner($user["uid"]);


            $this->entityManager->persist($reservation);
            $this->entityManager->flush();


            $rabbit = new RabbitMQLib($this->entityManager);
            $rabbit->Connection();
            $rabbit->declareQueue('test', false, true, false, false);
            $rabbit->sendData([$reservation->getUid()]);
            $rabbit->ConnectionClose();


            return $this->json([
                "uid" => $reservation->getUid(),
                "rank" => $reservation->getRank(),
                "status" => $reservation->getStatus(),
                "createdAt" => $reservation->getCreatedAt(),
                "updatedAt" => $reservation->getUpdatedAt(),
                "expiresAt" => $reservation->getExpiresAt()
            ]);
        } catch (Exception $e) {
            return $this->json($e->getMessage());
        }
    }
}
