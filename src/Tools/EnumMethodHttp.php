<?php

namespace App\Tools;

use Symfony\Component\HttpFoundation\Request;

enum EnumMethodHttp: string {
    case GET = Request::METHOD_GET;
    case HEAD = Request::METHOD_HEAD;
}