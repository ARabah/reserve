<?php

namespace App\Tools;

use App\Entity\Film;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;
use Symfony\Contracts\HttpClient\HttpClientInterface;

Class libMovis {
    private ?UuidV4 $uid = null;
    private ?string $name = null;

    public function __construct(private EntityManagerInterface $entityManager, private HttpClientInterface $client) {
    }

    public function getMovisUuid() : UuidV4 {
        return $this->uid;
    }

    public function setMovisUuid(mixed $uuid) : static {
        if (!((new Uuid($uuid))->isValid($uuid))) {
            throw new Exception("no valide", 500);
        }
        elseif (!preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid)) {
            throw new Exception("no valide", 500);
        }
        else {
            $this->uid = new UuidV4($uuid);
        }
        return $this;
    }

    public function getMovies() : array {
        $response = $this->client->request('GET', 'http://films/movies/' . $this->getMovisUuid(), ['headers' => ['Content-Type' => 'application/json']]);
        return json_decode($response->getContent(), true);
    }

    public function getMovisName() : string {
        return $this->name;
    }

    public function setMovisName(string $name) : static {
        if (strlen($name) > 128 && empty($name)) throw new Exception("no valide", 500);
        else $this->name;
        
        return $this;
    }

}