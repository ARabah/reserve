<?php

namespace App\Tools;

use App\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQLib {

    private ?AMQPStreamConnection $connection = null;
    private ?AMQPChannel $channel = null;
    private ?string $name = null;

    public function __construct(private EntityManagerInterface $entityManager) {
        $this->connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
    }

    public function Connection() : void {
        $this->channel = $this->connection->channel();
    }

    public function declareQueue(string $name, $passive=false, $durable=false, $exclusive=false, $auto_delete=true) : void {
        $this->name = $name;
        $this->channel->queue_declare($name, $passive, $durable, $exclusive, $auto_delete);
    }

    public function sendData(array $data = null) : void {
        if (empty($data) || $data == null) {
            $data = $this->entityManager->getRepository(Reservation::class)->findAll();
        }
        
        $msg = new AMQPMessage(
            implode(' ', $data),
            ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]
        );
        
        $this->channel->basic_publish($msg, '', $this->name);
    }

    public function ChannelClose() : void {
        $this->channel->close();
    }

    public function ConnectionClose() : void {
        $this->ChannelClose();
        $this->connection->close();
    }
}


