<?php

namespace App\Tools;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class OauthLib
{
    private ?string $authentication = null;
    private ?string $link = "http://oauth/api/account/me";
    private string $methode = EnumMethodHttp::HEAD->value;

    public function __construct(private HttpClientInterface $client) {
    }

    public function getAuthentication() : ?string {
        return $this->authentication;
    }

    public function setAuthentication(?string $authentication) : static {
        $this->authentication = $authentication;
        return $this;
    }


    public function getHeaderOauth() {
        $response = $this->client->request($this->methode, $this->link, [
            'headers' => [
                'authentication' => $this->getAuthentication()
            ]
        ]);
        return $response;
    }

    public function setMethod(EnumMethodHttp $method = EnumMethodHttp::HEAD) : static {
        $this->methode = $method->value;
        return $this;
    }

}
