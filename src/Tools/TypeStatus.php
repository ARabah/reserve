<?php

namespace App\Tools;

enum TypeStatus: string {
    case Open = "open";
    case Expired = "expired";
    case Confirmed = "confirmed";
    case Wait = "wait";
}