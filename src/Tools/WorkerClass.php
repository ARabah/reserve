<?php

namespace App\Tools;

use App\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Symfony\Component\Uid\UuidV4;
use Throwable;

class WorkerClass {
    private ?AMQPStreamConnection $connection = null;
    private $channel;
    private ?string $name = null;
    private ?UuidV4 $uid = null;

    public function __construct(private EntityManagerInterface $entityManager) {
        $this->connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');    
    }

    public function Connection() : void {
        $this->channel = $this->connection->channel();
    }

    public function declareQueue(string $name, $passive=false, $durable=false, $exclusive=false, $auto_delete=true) : void {
        $this->name = $name;
        $this->channel->queue_declare($name, $passive, $durable, $exclusive, $auto_delete);
    }

    public function verifReservationUID(UuidV4 $uid) {
        if (!UuidV4::isValid($uid)) throw new \Exception("Error Uuid", 404);
        if (empty($this->entityManager->getRepository(Reservation::class)->findBy(["uid" => $uid]))) throw new \Exception("Not fund Reservation", 404);
        $this->uid = $uid;
    }

    public function getData($callback) : void {
        // $callback = function ($msg) {
        //     dd($msg);
        //     sleep(substr_count($msg->getBody(), '.'));
        //     $msg->ack();
        // };

        $this->channel->basic_qos(null, 1, false);
        $this->channel->basic_consume($this->name, '', false, false, false, false, $callback);
    }

    public function Test() : Throwable {
        try {
            $this->channel->consume();
        } catch (\Throwable $th) {
            return $th;
        }
    }


    public function getUuidValue() : UuidV4 {
        return $this->uid;
    }

    public function ChannelClose() : void {
        $this->channel->close();
    }

    public function ConnectionClose() : void {
        $this->ChannelClose();
        $this->connection->close();
    }
}
