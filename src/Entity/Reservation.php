<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use App\Tools\TypeStatus;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'uuid')]
    private ?UuidV4 $uid = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $rank = null;

    #[ORM\Column(length: 255, enumType: TypeStatus::class)]
    private ?TypeStatus $status = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $seats = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $expiresAt = null;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    private ?Sceance $seance = null;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    private ?Room $room = null;

    #[ORM\Column(type: 'uuid', nullable: true)]
    private ?Uuid $Owner = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUid(): ?UuidV4
    {
        return $this->uid;
    }

    public function setUid(UuidV4 $uid): static
    {
        $this->uid = $uid;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(int $rank): static
    {
        $this->rank = $rank;

        return $this;
    }

    public function getStatus(): ?TypeStatus
    {
        return $this->status;
    }

    public function setStatus(TypeStatus $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getSeats(): ?int
    {
        return $this->seats;
    }

    public function setSeats(int $seats): static
    {
        $this->seats = $seats;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(\DateTimeInterface $expiresAt): static
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    public function getSeance(): ?Sceance
    {
        return $this->seance;
    }

    public function setSeance(?Sceance $seance): static
    {
        $this->seance = $seance;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): static
    {
        $this->room = $room;

        return $this;
    }

    public function getOwner(): ?Uuid
    {
        return $this->Owner;
    }

    public function setOwner(Uuid $Owner): static
    {
        $this->Owner = $Owner;

        return $this;
    }
}
