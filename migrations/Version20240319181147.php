<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240319181147 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reservation ADD seance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE reservation ADD room_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955E3797A94 FOREIGN KEY (seance_id) REFERENCES sceance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C8495554177093 FOREIGN KEY (room_id) REFERENCES room (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_42C84955E3797A94 ON reservation (seance_id)');
        $this->addSql('CREATE INDEX IDX_42C8495554177093 ON reservation (room_id)');
        $this->addSql('ALTER TABLE room ADD sceance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519B57447AA6 FOREIGN KEY (sceance_id) REFERENCES sceance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_729F519B57447AA6 ON room (sceance_id)');
        $this->addSql('ALTER TABLE sceance RENAME COLUMN ddate TO date');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE sceance RENAME COLUMN date TO ddate');
        $this->addSql('ALTER TABLE reservation DROP CONSTRAINT FK_42C84955E3797A94');
        $this->addSql('ALTER TABLE reservation DROP CONSTRAINT FK_42C8495554177093');
        $this->addSql('DROP INDEX IDX_42C84955E3797A94');
        $this->addSql('DROP INDEX IDX_42C8495554177093');
        $this->addSql('ALTER TABLE reservation DROP seance_id');
        $this->addSql('ALTER TABLE reservation DROP room_id');
        $this->addSql('ALTER TABLE room DROP CONSTRAINT FK_729F519B57447AA6');
        $this->addSql('DROP INDEX IDX_729F519B57447AA6');
        $this->addSql('ALTER TABLE room DROP sceance_id');
    }
}
